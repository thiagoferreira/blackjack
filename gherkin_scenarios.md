**Scenario**: Player get 21 score

    Given the player has started the game
    When is getting cards until getting 21
    And get 21 score
    Then is displayed a message that the player won
    
**Scenario**: Player get more than 21 score

    Given the player has started the game
    When is getting cards until getting 21
    And get more than 21 score
    Then is displayed a message that the player losed
    
**Scenario**: Player 1 get more score than the player 2

    Given the player 1  has finished your turn with less than 21 score
    When the player 2 finish your turn with less score than player 1
    Then is displayed a message that player 1 was the winner

**Scenario**: Tie in the game

    Given the player 1  has finished your turn with less than 21 score
    When the player 2 finish your turn with the same score than player 1
    Then is displayed a message that was a tie
