from random import randint

random1 = randint(1, 10)
random2 = randint(1, 10)
player_1 = random1 + random2
player_2 = 0
exploded = False
score21 = False


def get_random_number():
    ramdom_number = randint(1, 10)
    return ramdom_number

def give_cards_or_stop(player_score):
    global decision
    while player_score < 21:
        decision = input('Do you want another card? type 1 for Yes and 2 for Not \n')
        if decision == '1' or decision == '2':
          if decision == '1':
            random_number = get_random_number()
            player_score += random_number
            print(f'Recieved the card {random_number} ... total until now: {player_score}')
          elif decision == '2':
            print('You stopped here! \n')
            break
        else:
            print('It just allow 1 or 2')
    return player_score

def validate_who_wins(player_1_score,player_2_score):
    print(f'Total Player 1 {player_1_score} \nTotal Player 2 {player_2_score} \n')
    if player_1_score > 21:
      print("---------------------------------------------")
      print('               Player 2 Wins')
      print("---------------------------------------------")
    elif player_2_score > 21:
      print("---------------------------------------------")
      print('               Player 1 Wins')
      print("---------------------------------------------")
    elif player_1_score > player_2_score:
      print("---------------------------------------------")
      print('               Player 1 Wins')
      print("---------------------------------------------")
    elif player_1_score == player_2_score:
      print("---------------------------------------------")
      print('                 A tie')
      print("---------------------------------------------")
    elif player_1_score < player_2_score:
      print("---------------------------------------------")
      print('               Player 2 Wins')
      print("---------------------------------------------")

def exploded_or_win(player_score):
    if player_score > 21:
      print("---------------------------------------------")
      print('         You got more than 21, You Lost')
      print("---------------------------------------------")
      exploded = True
      return exploded
    elif player_score == 21:
      print("---------------------------------------------")
      print('                 You Win                     ')
      print("---------------------------------------------")
      score21 = True
      return score21
    else:
      exploded = False
      return exploded

if __name__ == '__main__':
  run = '.'
  while run != '':
    run = input('Press Enter to start the game \n')

  print('Game started...')
  print(f'You received {random1} and {random2} Total of: {player_1} ')
      
  while exploded == False and score21 == False:
      player_1 = give_cards_or_stop(player_1)    
      exploded = exploded_or_win(player_1)
      if decision == '2':
        exploded = False 
        break
    
  while exploded == False and score21 == False:
      print('------ SECOND PLAYER TURN --------- \n')
      random1 = get_random_number()
      random2 = get_random_number()
      player_2 = random1+random2
      print(f'You received {random1} and {random2} Total of: {player_2} ')
      player_2 = give_cards_or_stop(player_2)
      exploded = exploded_or_win(player_2)
      if decision == '2':
          exploded = True

  validate_who_wins(player_1,player_2)