from blackjack import get_random_number, give_cards_or_stop, validate_who_wins, exploded_or_win
from unittest import TestCase, main
from unittest.mock import patch
from io import StringIO

class Tests_Exploded_or_Win(TestCase):
    def test_if_score_lower_than_21_returns_false(self):
      self.assertFalse(exploded_or_win(20))

    def test_if_score_equals_21_returns_true(self):
      self.assertTrue(exploded_or_win(21))
    
    def test_if_score_greater_than_21_returns_true(self):
      self.assertTrue(exploded_or_win(22))

    def test_if_explode_is_showing_lose_message(self):
      with patch('sys.stdout', new=StringIO()) as fake_out:
       exploded_or_win(22)
       self.assertIn('You got more than 21, You Lost',fake_out.getvalue())
    
    @patch('builtins.input')
    def test_if_win_is_showing_victory_message(self,mock_input):
      with patch('sys.stdout', new=StringIO()) as fake_out:
       exploded_or_win(21)
       self.assertIn('You Win',fake_out.getvalue())

class Tests_get_random_number(TestCase):
    def test_if_returns_a_number_lower_or_equal_10(self):
      self.assertLessEqual(get_random_number(),10)

    def test_if_returns_a_number_equal_or_greater_1(self):
      self.assertGreaterEqual(get_random_number(),1)  
  
class Tests_give_cards_or_stop(TestCase):
    def test_return_score_equal_21(self):
      self.assertEqual(give_cards_or_stop(21), 21)

    @patch('builtins.input', return_value='2')
    def test_if_dont_get_more_cards_return_the_same_value(self,mock_input):
      self.assertEqual(give_cards_or_stop(10), 10)
    
    @patch('builtins.input', return_value='2')
    def test_if_anwser_with_2_return_stop_message(self,mock_input):
      with patch('sys.stdout', new=StringIO()) as fake_out:
        give_cards_or_stop(10)
        self.assertEqual(fake_out.getvalue(), 'You stopped here! \n\n')

    @patch('builtins.input', side_effect=['1', '2'])
    def test_get_a_card_and_stop(self,mock_input):
      self.assertGreater(give_cards_or_stop(10), 10)
    
    @patch('builtins.input', side_effect=['a', '2'])
    def test_insert_a_different_value_of_1_or_2(self,mock_input):
      with patch('sys.stdout', new=StringIO()) as fake_out:
        give_cards_or_stop(10)
        self.assertEqual(fake_out.getvalue(), 'It just allow 1 or 2\nYou stopped here! \n\n')

    @patch('builtins.input', return_value='1')
    def test_if_received_card_message_is_correct(self,mock_input):
      with patch('sys.stdout', new=StringIO()) as fake_out:
       give_cards_or_stop(10)
       self.assertIn('Recieved the card',fake_out.getvalue())

    @patch('builtins.input', return_value='1')
    def test_if_received_card_message_is_correct2(self,mock_input):
      with patch('sys.stdout', new=StringIO()) as fake_out:
       give_cards_or_stop(10)
       self.assertIn('total until now:',fake_out.getvalue())

class Tests_validate_who_wins(TestCase):
    def test_display_score_player1_and_player2(self):
      with patch('sys.stdout', new=StringIO()) as fake_out:
        validate_who_wins(21,10)
        self.assertIn('Total Player 1 21 \nTotal Player 2 10 \n', fake_out.getvalue())

    def test_display_player2_won_when_player1_greater_than_21(self):
      with patch('sys.stdout', new=StringIO()) as fake_out:
        validate_who_wins(22,10)
        self.assertIn('Player 2 Wins\n', fake_out.getvalue())

    def test_display_player1_won_when_player2_greater_than_21(self):
      with patch('sys.stdout', new=StringIO()) as fake_out:
        validate_who_wins(10,22)
        self.assertIn('Player 1 Wins\n', fake_out.getvalue())

    def test_display_player1_won_when_player1_greater_than_player2(self):
      with patch('sys.stdout', new=StringIO()) as fake_out:
        validate_who_wins(20,19)
        self.assertIn('Player 1 Wins\n', fake_out.getvalue())

    def test_display_tie_when_player1_equals_player2(self):
      with patch('sys.stdout', new=StringIO()) as fake_out:
        validate_who_wins(20,20)
        self.assertIn('A tie', fake_out.getvalue())

    def test_display_player2_won_when_player2_greater_than_player1(self):
      with patch('sys.stdout', new=StringIO()) as fake_out:
        validate_who_wins(15,19)
        self.assertIn('Player 2 Wins\n', fake_out.getvalue())
    
    def test_display_player1_won_when_gets_21(self):
      with patch('sys.stdout', new=StringIO()) as fake_out:
        validate_who_wins(21,19)
        self.assertIn('Player 1 Wins\n', fake_out.getvalue())

    def test_display_player2_won_when_gets_21(self):
      with patch('sys.stdout', new=StringIO()) as fake_out:
        validate_who_wins(20,21)
        self.assertIn('Player 2 Wins\n', fake_out.getvalue())

if __name__ == '__main__':
    main()