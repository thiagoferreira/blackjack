# Blackjack Campinas Version xD

# Welcome
This is my code trying to do the blackjack game,  this is for study purposes only.

This is a game for two people.

*   Player 1 go start with two cards, can stop with two cards or get more cards, one per time, you can stop after getting a new card unless if you pass of 21, so you lose.

*   Player 2 goes starts after player 1 finishes your turn, the rules are the same to player 1.

*   After the second player finishes your turn, it is displayed who wins.

*   If any player gets a total of 21, won the game and the game ends immediately.

*   If any player gets more than 21, lost and the game ends immediately.

# How to run the game:

Is necessary has Python installed in your machine.
- Clone the repository
- In a terminal, go to the project folder and execute the command `python blackjack.py`

**How to run Unit Tests:**

- In a terminal, go to the project folder and execute the command `python -m unittest -v tests.py`

**Acknowledgments:**

Thanks to @ancarvalho for help in solving some problems and code review(first commit).

Thanks to Wagner Torres for review the Gherkin scenarios.

Content about unit test learned on [Live de Python](https://www.youtube.com/c/EduardoMendes/) channel

Content about mutation testing learned on [Live de Python](https://www.youtube.com/c/EduardoMendes/) channel and [https://dev.to/paulogoncalvesbh/testes-de-mutacao-1c7p](https://dev.to/paulogoncalvesbh/testes-de-mutacao-1c7p)